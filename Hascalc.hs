{- |
Module      :  Hascalc
Description :  Auxiliary module that holds code that I have no idea where I should drop it else
Copyright   :  (c) Norbert Melzer
License     :  GPL-v3

Maintainer  :  <timmelzer@gmail.com>
Stability   :  experimental
Portability :  portable

No discription so far!
-}

module Hascalc where

import           Hascalc.Data.Numeric (Numeric (..))
import           Hascalc.Expression   (Expr (..))
import           Hascalc.Operator     (Operator (..), UOperator (..))

class Evaluable a where
  eval :: a -> a

instance Evaluable Expr where
  eval (Const i)      = Const i
  eval (BinOp op l r) = applyOp  op (eval l) (eval r)
  eval (UnOp  op e)   = applyUOp op (eval e)

-- | applys an `Operator` to an `Expr`
applyOp                                              :: Operator -> Expr -> Expr -> Expr
applyOp Add  (Const (Integer a)) (Const (Integer b)) = Const . Integer $ a + b
applyOp Add  (Const (Integer a)) (Const (Float   b)) = Const . Float   $ fromIntegral a + b
applyOp Add  (Const (Float   a)) (Const (Integer b)) = Const . Float   $ a + fromIntegral b
applyOp Add  (Const (Float   a)) (Const (Float   b)) = Const . Float   $ a + b
applyOp Add  left                right               = applyOp Add  (eval left) (eval right)

applyOp Sub  (Const (Integer a)) (Const (Integer b)) = Const . Integer $ a - b
applyOp Sub  (Const (Integer a)) (Const (Float   b)) = Const . Float   $ fromIntegral a - b
applyOp Sub  (Const (Float   a)) (Const (Integer b)) = Const . Float   $ a - fromIntegral b
applyOp Sub  (Const (Float   a)) (Const (Float   b)) = Const . Float   $ a - b
applyOp Sub  left                right               = applyOp Sub  (eval left) (eval right)

applyOp Mult (Const (Integer a)) (Const (Integer b)) = Const . Integer $ a * b
applyOp Mult (Const (Integer a)) (Const (Float   b)) = Const . Float   $ fromIntegral a * b
applyOp Mult (Const (Float   a)) (Const (Integer b)) = Const . Float   $ a * fromIntegral b
applyOp Mult (Const (Float   a)) (Const (Float   b)) = Const . Float   $ a * b
applyOp Mult left                right               = applyOp Mult (eval left) (eval right)

applyOp Div  (Const (Integer a)) (Const (Integer b)) | a `mod` b == 0 = Const . Integer $ a `div` b
                                                     | otherwise     = Const . Float   $ fromIntegral a / fromIntegral b
applyOp Div  (Const (Integer a)) (Const (Float   b)) = Const . Float   $ fromIntegral a / b
applyOp Div  (Const (Float   a)) (Const (Integer b)) = Const . Float   $ a / fromIntegral b
applyOp Div  (Const (Float   a)) (Const (Float   b)) = Const . Float   $ a / b
applyOp Div  left                right               = applyOp Div  (eval left) (eval right)

applyOp Pow  (Const (Integer a)) (Const (Integer b)) = Const . Integer $ a^b
applyOp Pow  (Const (Integer a)) (Const (Float   b)) = Const . Float   $ fromIntegral a**b
applyOp Pow  (Const (Float   a)) (Const (Integer b)) = Const . Float   $ a^b
applyOp Pow  (Const (Float   a)) (Const (Float   b)) = Const . Float   $ a**b
applyOp Pow  left                right               = applyOp Pow  (eval left) (eval right)

applyOp Bin1 left                right               = applyOp Pow  (BinOp Add left right) (Const (Integer 2))
applyOp Bin2 left                right               = applyOp Pow  (BinOp Sub left right) (Const (Integer 2))
applyOp Bin3 left                right               = applyOp Mult (BinOp Add left right) (BinOp Sub left right)

applyUOp                         :: UOperator -> Expr -> Expr
applyUOp Fac (Const (Integer n)) = Const . Integer $ product [1..n]
applyUOp Fac (Const (Float   _)) = error "Can't calculate faculty of a Float!"

valOf                     :: Expr -> Double
valOf (Const (Integer i)) = fromIntegral i
valOf (Const (Float   f)) = f

intValOf                     :: Expr -> Maybe Integer
intValOf (Const (Integer i)) = Just i
intValOf (Const (Float   f)) | isInt f 7 = Just $ truncate f
                             | otherwise = Nothing

-- | Returns if x is an int to n decimal places
isInt     :: (RealFrac a) => a -> Int -> Bool
isInt x n = (round $ 10^(fromIntegral n)*(x-(fromIntegral $ round x)))==0
