module Main where

import           Hascalc
import           Hascalc.Data.Numeric
import           Hascalc.Expression
import           Hascalc.Operator
import           System.Exit          (ExitCode (..), exitSuccess, exitWith)
import           Test.HUnit

main :: IO ()
main = do
  testSum <- runTest
  case failures testSum of
    0 -> exitSuccess
    n -> exitWith (ExitFailure n)

runTest :: IO Counts
runTest = runTestTT (test testListe)
  where
    testListe = [
      -- Showing Expressions
      "Show 3"       ~: (show . Const $ Integer 3)                                  ~?= "3",
      "Show 3.5"     ~: (show . Const $ Float   3.5)                                ~?= "3.5",
      "Show (6+3)"   ~: (show $ BinOp Add  (Const (Integer 6)) (Const (Integer 3))) ~?= "(6+3)",
      "Show (6-3)"   ~: (show $ BinOp Sub  (Const (Integer 6)) (Const (Integer 3))) ~?= "(6-3)",
      "Show (6/3)"   ~: (show $ BinOp Div  (Const (Integer 6)) (Const (Integer 3))) ~?= "(6/3)",
      "Show (6*3)"   ~: (show $ BinOp Mult (Const (Integer 6)) (Const (Integer 3))) ~?= "(6*3)",
      "Show (6^3)"   ~: (show $ BinOp Pow  (Const (Integer 6)) (Const (Integer 3))) ~?= "(6^3)",
      "Show (6<+>3)" ~: (show $ BinOp Bin1 (Const (Integer 6)) (Const (Integer 3))) ~?= "(6<+>3)",
      "Show (6<->3)" ~: (show $ BinOp Bin2 (Const (Integer 6)) (Const (Integer 3))) ~?= "(6<->3)",
      "Show (6<*>3)" ~: (show $ BinOp Bin3 (Const (Integer 6)) (Const (Integer 3))) ~?= "(6<*>3)",
      "Show 6!"      ~: (show $ UnOp  Fac  (Const (Integer 6))                    ) ~?= "6!",
      -- Parsing Expressions
      "readExpr 3"       ~: (read "3"       :: Expr) ~?= Const (Integer 3  ),
      "readExpr 3.5"     ~: (read "3.5"     :: Expr) ~?= Const (Float   3.5),
      "readExpr (6+3)"   ~: (read "(6+3)"   :: Expr) ~?= BinOp Add  (Const (Integer 6)) (Const (Integer 3)),
      "readExpr (6-3)"   ~: (read "(6-3)"   :: Expr) ~?= BinOp Sub  (Const (Integer 6)) (Const (Integer 3)),
      "readExpr (6*3)"   ~: (read "(6*3)"   :: Expr) ~?= BinOp Mult (Const (Integer 6)) (Const (Integer 3)),
      "readExpr (6/3)"   ~: (read "(6/3)"   :: Expr) ~?= BinOp Div  (Const (Integer 6)) (Const (Integer 3)),
      "readExpr (6^3)"   ~: (read "(6^3)"   :: Expr) ~?= BinOp Pow  (Const (Integer 6)) (Const (Integer 3)),
      "readExpr (6<+>3)" ~: (read "(6<+>3)" :: Expr) ~?= BinOp Bin1 (Const (Integer 6)) (Const (Integer 3)),
      "readExpr (6<->3)" ~: (read "(6<->3)" :: Expr) ~?= BinOp Bin2 (Const (Integer 6)) (Const (Integer 3)),
      "readExpr (6<*>3)" ~: (read "(6<*>3)" :: Expr) ~?= BinOp Bin3 (Const (Integer 6)) (Const (Integer 3)),
      "readExpr [6]!"    ~: (read "[6]!"    :: Expr) ~?= UnOp  Fac  (Const (Integer 6)),
      -- Evaluating Expressions
      "eval (6+3)"   ~: eval (read "(2+3)"   :: Expr) ~?= Const (Integer   5),
      "eval (6-3)"   ~: eval (read "(6-3)"   :: Expr) ~?= Const (Integer   3),
      "eval (6/3)"   ~: eval (read "(6/3)"   :: Expr) ~?= Const (Integer   2),
      "eval (6*3)"   ~: eval (read "(6*3)"   :: Expr) ~?= Const (Integer  18),
      "eval (6^3)"   ~: eval (read "(6^3)"   :: Expr) ~?= Const (Integer 216),
      "eval (6<+>3)" ~: eval (read "(6<+>3)" :: Expr) ~?= Const (Integer  81),
      "eval (6<->3)" ~: eval (read "(6<->3)" :: Expr) ~?= Const (Integer   9),
      "eval (6<*>3)" ~: eval (read "(6<*>3)" :: Expr) ~?= Const (Integer  27),
      "eval [6]!"    ~: eval (read "[6]!"    :: Expr) ~?= Const (Integer 720),
      -- other Tests
      "valOf 5"    ~: valOf (Const (Integer 5))               ~?= 5.0,
      "intValOf 5" ~: intValOf (Const (Integer 5))            ~?= Just 5,
      "(2-1)=1"    ~: (intValOf . eval) (read "(2-1)" :: Expr) ~?= Just 1,
      "read Int"   ~: (read "2" :: Expr)                       ~?= Const (Integer 2)]

