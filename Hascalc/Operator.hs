module Hascalc.Operator (Operator(..), UOperator(..)) where

data Operator = Add
              | Mult
              | Sub
              | Div
              | Pow
              | Bin1
              | Bin2
              | Bin3
              deriving (Eq)

instance Show Operator where
  show Add  = "+"
  show Mult = "*"
  show Sub  = "-"
  show Div  = "/"
  show Pow  = "^"
  show Bin1 = "<+>"
  show Bin2 = "<->"
  show Bin3 = "<*>"

instance Read Operator where
  readsPrec _ (o:[])         = case o of
                                 '+' -> [(Add , "")]
                                 '-' -> [(Sub , "")]
                                 '*' -> [(Mult, "")]
                                 '/' -> [(Div , "")]
                                 '^' -> [(Pow , "")]
                                 _   -> error "Not a valid operator"
  readsPrec _ ('<':o:'>':[]) = case o of
                                 '+' -> [(Bin1, "")]
                                 '-' -> [(Bin2, "")]
                                 '*' -> [(Bin3, "")]
                                 _   -> error "Not a valid operator"
  readsPrec _ (_:_)          = error "Not a valid operator"

-- TODO: Später mit Parsec einlesen!

data UOperator = Fac
               deriving (Eq)

instance Show UOperator where
  show Fac = "!"

instance Read UOperator where
  readsPrec _ (o:[]) = case o of
                        '!' -> [(Fac, "")]
                        _   -> error "Not a valid operator"
  readsPrec _ (_:_)  = error "Not a valid operator"
