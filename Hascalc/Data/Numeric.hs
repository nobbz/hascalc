module Hascalc.Data.Numeric (Numeric(..), parseNumeric) where

import           Control.Monad
import           Text.ParserCombinators.Parsec

data Numeric = Integer Integer
             | Float   Double
             deriving (Eq)

instance Show Numeric where
  show (Integer i) = show i
  show (Float   f) = show f

parseNumeric :: Parser Numeric
parseNumeric = try parseFloat <|> parseInteger

parseInteger :: Parser Numeric
parseInteger = liftM (Integer . read) $ many1 digit

parseFloat :: Parser Numeric
parseFloat = do
  before <- many1 digit
  char '.'
  after <- many1 digit
  return . Float . read $ before ++ '.':after
