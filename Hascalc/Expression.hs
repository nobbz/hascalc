module Hascalc.Expression (Expr(Const, BinOp, UnOp)) where

import           Hascalc.Data.Numeric
import           Hascalc.Operator
import           Text.ParserCombinators.Parsec

data Expr     = Const Numeric
              | BinOp Operator  Expr Expr
              | UnOp  UOperator Expr
              deriving (Eq)

instance Show Expr where
  show expr = showsExpr expr ""

instance Read Expr where
  readsPrec _ s = [(readExpr s, "")]

showsExpr                  :: Expr -> String -> String
showsExpr (Const n)      s = shows n s
showsExpr (BinOp op l r) s = '(': showsExpr l (show op ++ showsExpr r (')':s))
showsExpr (UnOp  op e)   s = showsExpr e (head(show op):s)

readExpr       :: String -> Expr
readExpr input = case parse parseExpr "" input of
  Left  err  -> error $ show err
  Right expr -> expr

parseExpr :: Parser Expr
parseExpr = do
  expr <- try parseBinOp
     <|> try parseUnOp
     <|> parseConst
  return expr

parseConst :: Parser Expr
parseConst = do
  numeric <- parseNumeric
  return $ Const numeric

parseBinOp :: Parser Expr
parseBinOp = do
  char '('
  left  <- parseExpr
  op    <- parseOp
  right <- parseExpr
  char ')'
  return $ BinOp op left right

parseUnOp :: Parser Expr
parseUnOp = do
  char '['
  operand  <- parseExpr
  char ']'
  operator <- parseUOp
  return $ UnOp operator operand

parseUOp :: Parser UOperator
parseUOp = do
  char '!'
  return Fac

parseOp :: Parser Operator
parseOp = do
  op <-
    try (string "<+>") <|>
    try (string "<->") <|>
    string      "<*>"  <|>
    string      "+"    <|>
    string      "-"    <|>
    string      "*"    <|>
    string      "/"    <|>
    string      "^"
  return $ case op of
    "+"   -> Add
    "-"   -> Sub
    "*"   -> Mult
    "/"   -> Div
    "^"   -> Pow
    "<+>" -> Bin1
    "<->" -> Bin2
    "<*>" -> Bin3
